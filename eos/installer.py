#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule handling the installation of packages."""

import subprocess

from .manifest import RationalisedManifest
from .logger import Logger


class Installer:
    """A class to assist in setting up a new linux distro with dev tools."""

    def __init__(self, manifest: RationalisedManifest, logger: Logger):
        """Initialise the installer object."""
        self.manifest = manifest
        self.logger = logger

    def get_managers_for_package(self, name: str) -> [str]:
        """Get the manager name for a given package."""
        ret = []
        # Get package info
        package = self.manifest.packages.get(name)
        if package is None:
            raise ValueError(f"Package `{name}` not found")
        # Get install info for managers
        for manager_name, manager_info in self.manifest.managers.items():
            pkg_info_for_manager = package.get(manager_name)
            if pkg_info_for_manager is not None:
                # # Check version
                version = manager_info.get("version")
                min_version = pkg_info_for_manager.get("minimum_version")
                if min_version is not None and min_version.isnumeric():
                    if version is not None and version.isnumeric():
                        if float(version) >= float(min_version):
                            ret.append(manager_name)
                else:
                    ret.append(manager_name)
        # Get install info for `custom`
        custom_info = package.get("custom")
        if custom_info is not None:
            ret.append("custom")
        # Reorder according to `prefer`
        prefer_list = package.get("prefer")
        if prefer_list is not None:
            # # Sort `ret`
            prefer_order = {k: v for v, k in enumerate(prefer_list)}
            ret.sort(key=prefer_order.get)
        # Return
        self.logger.debug(f"get_managers_for_package({name})\n"
                          f"  {ret}")
        return ret

    def get_managers_for_packages(self, names: [str]) -> {str: [str]}:
        """Get a dictionary of associations of packages to managers."""
        ret = {}
        for name in names:
            valid_managers = self.get_managers_for_package(name)
            if len(valid_managers) == 0:
                raise ValueError(f"No managers found for `{name}`")
            if ret.get(valid_managers[0]) is None:
                ret[valid_managers[0]] = [name]
            else:
                ret[valid_managers[0]].append(name)
        # Return
        self.logger.debug(f"get_managers_for_packages({names})\n"
                          f"  {ret}")
        return ret

    def install_packages(self, names: [str]):
        """Install a list of packages on any manager that can get them."""
        self.logger.debug(f"install_packages({names})")
        # Get manager dictionary
        managers = self.get_managers_for_packages(names)
        # Install
        if managers.get("custom") is not None:
            for v in managers["custom"]:
                custom_info = self.manifest.packages[v]["custom"]
                commands = custom_info["commands"]
                sudos = custom_info["sudos"]
                sudos = [self.manifest.sudo if sudo else "" for sudo in sudos]
                commands = [f"{sudo} {command}" for sudo, command in
                            zip(sudos, commands)]
                for command in commands:
                    self.logger.debug(f"install_packages({names})\n"
                                      f"  {v}: {command}")
                    subprocess.run(
                        command,
                        shell=True)
        for k, vs in managers.items():
            if k == "custom":
                continue
            manager = self.manifest.managers[k]
            vs = [self.manifest.packages[name][k]['package'] for name in vs]
            if manager['sudo']:
                command = f"{self.manifest.sudo} "
            else:
                command = ""
            command += (
                f"{manager['manager']} {manager['install']} {' '.join(vs)}")
            self.logger.debug(f"install_packages({names})\n"
                              f"  {k}, {vs}: {command}")
            self.logger.info("\n"
                             f"Installing [italic fg=blue]`{' '.join(vs)}`[/] "
                             f"from [italic fg=magenta]`{manager['manager']}`"
                             "[/]:\n")
            subprocess.run(
                command,
                shell=True)
            self.logger.info("\nSuccessfully installed from "
                             f"[italic fg=magenta]`{manager['manager']}`[/]\n")
