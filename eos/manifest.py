#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule handling the reading of json manifests."""

import json
import os
import pathlib
import shutil
from typing import Union


def format_title(title: str, indent: int = 2) -> str:
    """Apply formatting to a section title."""
    return f"{' ' * indent}[bold underline fg=blue]{title}[/]"


def format_subtitle(subtitle: str, indent: int = 4) -> str:
    """Apply formatting to a section subtitle."""
    return f"{' ' * indent}[bold fg=red]{subtitle}[/]"


def format_binary(binary: str) -> str:
    """Apply formatting to one or more binary names."""
    return f"[italic fg=magenta]{binary}[/]"


def format_flags(flags: str) -> str:
    """Apply formatting to one or more arguments to a binary."""
    return f"[italic fg=blue]{flags}[/]"


def format_list(strs: [str]) -> str:
    """Apply formatting to a list of strings."""
    return "`" + ("`, `".join([f"[italic fg=green]{s}[/]" for s in strs])) + "`"  # noqa


def merge_dicts(left: Union[dict, None],
                right: Union[dict, None]) -> Union[dict, None]:
    """Merge two dictionaries that may or may not be `None`."""
    if left is None:
        return right
    if right is None:
        return left
    return {**left, **right}


def merge_lists(left: Union[list, None],
                right: Union[list, None]) -> Union[list, None]:
    """Merge two lists that may or may not be `None`."""
    if left is None:
        return right
    if right is None:
        return left
    return left + right


class Manifest:
    """A class to parse JSON data representing installation information."""

    def __init__(self, distros, managers, packages, sudos):
        """Initialize the class."""
        # Write to object
        self.distros = distros
        self.managers = managers
        self.packages = packages
        self.sudos = sudos

    @classmethod
    def from_json_file(cls, file: Union[str, pathlib.Path]):
        """Create a new Manifest object from a JSON file."""
        # Get the data from the file
        with open(file) as json_file:
            data = json.load(json_file)
        # Return
        return cls(
            data.get("distros"),
            data.get("managers"),
            data.get("packages"),
            data.get("sudos"))

    @classmethod
    def merge(cls, left, right):
        """Merge two manifests."""
        return cls(
            merge_dicts(left.distros, right.distros),
            merge_dicts(left.managers, right.managers),
            merge_dicts(left.packages, right.packages),
            merge_lists(left.sudos, right.sudos))

    @classmethod
    def from_json_files(cls, files: [Union[str, pathlib.Path]]):
        """Create a new Manifest object from a list of JSON files."""
        manifests = [cls.from_json_file(file) for file in files]
        if len(manifests) == 0:
            return None
        if len(manifests) == 1:
            return manifests[0]
        ret = manifests[0]
        for manifest in manifests[1:]:
            ret = cls.merge(ret, manifest)
        return ret

    def validate(self):
        """Check that the manifest is minimally valid."""
        # Validate distros
        if self.distros is None:
            raise ValueError("No `distros` found in JSON file")
        for k, v in self.distros.items():
            manager_str = v.get("manager")
            if manager_str is None:
                raise ValueError(f"No `manager` found for distro `{k}`")
            install_str = v.get("install")
            if install_str is None:
                raise ValueError(f"No `install` found for distro `{k}`")
        # Validate managers
        if self.managers is None:
            raise ValueError("No `managers` found in JSON file")
        for k, v in self.managers.items():
            install_str = v.get("install")
            if install_str is None:
                raise ValueError(f"No `install` found for manager `{k}`")
        # Validate packages
        if self.packages is None:
            raise ValueError("No `packages` found in JSON file")
        # Validate sudos
        if self.sudos is None:
            raise ValueError("No `sudos` found in JSON file")


class RationalisedManifest:
    """A class to rationalise the installation information."""

    def __init__(self,
                 distro_id: str,
                 distro_version: str,
                 manifest: Manifest):
        """Initialize the class."""
        # Validate the manifest
        manifest.validate()
        # Rationalise `managers`
        self.managers = manifest.managers
        # # Find the `manager` binaries
        self._removed_managers = []
        for k, v in self.managers.items():
            v["manager"] = v.get("manager", k)
            manager = shutil.which(v["manager"])
            if manager is None:
                self._removed_managers.append(k)
            else:
                v["manager"] = manager
        # # Add the correct distro manager to `managers`
        # # # Select specific distro
        distro = manifest.distros.get(distro_id)
        if distro is None:
            raise ValueError(f"Distro not found: `{distro_id}`")
        self.managers["distro"] = distro
        self.managers["distro"]["version"] = distro_version
        # # # Find the `manager` binary
        manager = shutil.which(self.managers["distro"]["manager"])
        if manager is None:
            raise ValueError(f"Binary `{self.managers['distro']['manager']}` "
                             "not found")
        self.managers["distro"]["manager"] = manager
        # # # Create list of all removed distros
        removed_distros = manifest.distros
        removed_distros.pop(distro_id)
        # # Delete managers that couldn't be found
        for k in self._removed_managers:
            self.managers.pop(k)
        self._removed_managers.extend(removed_distros)

        # Rationalise `packages`
        self.packages = manifest.packages
        for pkg_name, pkg_info in self.packages.items():
            # # Move the distro manager to `distro`
            if pkg_info.get(distro_id) is not None:
                pkg_info["distro"] = pkg_info.pop(distro_id)
            # # Delete managers that couldn't be found
            for manager_name in self._removed_managers:
                if pkg_info.get(manager_name) is not None:
                    pkg_info.pop(manager_name)
            # # Add `package` to all managers
            for manager_name, manager_info in self.managers.items():
                if pkg_info.get(manager_name) is not None:
                    if pkg_info[manager_name].get("package") is None:
                        pkg_info[manager_name]["package"] = pkg_name
            # # Reorganise `prefer`
            prefer_list = pkg_info.get("prefer")
            if prefer_list is not None:
                # # # Move the distro manager to `distro`
                try:
                    index = prefer_list.index(distro_id)
                except ValueError:
                    index = None
                if index is not None:
                    prefer_list.remove(distro_id)
                    prefer_list.insert(index, "distro")
                # # # Remove invalid managers
                for manager_name in self._removed_managers:
                    if manager_name in prefer_list:
                        prefer_list.remove(manager_name)
                # # # Save
                pkg_info["prefer"] = prefer_list

        # Rationalise `sudos`
        for sudo in manifest.sudos:
            self.sudo = shutil.which(sudo)
            if self.sudo is not None:
                break
        if self.sudo is None:
            if os.geteuid() != 0:
                raise ValueError("Sudo giver couldn't be found: "
                                 f"`{manifest.sudos}`")
            else:
                self.sudo = ""

    @classmethod
    def from_json_file(cls,
                       distro_id: str,
                       distro_version: str,
                       file: Union[str, pathlib.Path]):
        """Create a new RationalisedManifest object from a JSON file."""
        return cls(
            distro_id,
            distro_version,
            Manifest.from_json_file(file))

    @classmethod
    def from_json_files(cls,
                        distro_id: str,
                        distro_version: str,
                        files: [Union[str, pathlib.Path]]):
        """Create a new RationalisedManifest object from list of JSON files."""
        return cls(
            distro_id,
            distro_version,
            Manifest.from_json_files(files))

    def brief_summary(self) -> str:
        """Return a brief description of the RationalisedManifest."""
        ret = ""
        # Managers
        ret += f"\n{format_title('Managers')}[/]\n"
        for manager_name, manager_info in self.managers.items():
            ret += f"{format_subtitle(manager_name)}\n"
            if manager_info["sudo"]:
                sudo = f"{format_binary(self.sudo)} "
            else:
                sudo = ""
            binary = format_binary(manager_info['manager'])
            flags = format_flags(manager_info['install'])
            ret += f"      {sudo}{binary} {flags}\n"
        # Packages
        ret += f"\n{format_title('Packages')}[/]\n"
        for package_name, package_info in self.packages.items():
            ret += f"{format_subtitle(package_name)}\n"
            if "prefer" in package_info:
                from_list = package_info['prefer']
            else:
                from_list = [key for key in package_info.keys()
                             if key in self.managers.keys()]
            ret += f"      {format_list(from_list)}\n"
        # Return
        return ret
