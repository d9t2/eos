#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule enabling colour formatting of strings.

Functions in this submodule enable a string to specify visual formatting to be
applied to subsections of the string. This formatting is given inline within
the string, by enclosing it in square brackets, similar to a tmux config.

For example:

    "This is my [bold underline fg=red]partially formatted string[/]"

The list of available formatting strings:

    * `/`
        * Use this to reset back to normal formatting.
    * `regular`
        * Same as `/`.
    * `bold`
    * `dim`
    * `italic`
    * `underline`
    * `strikethrough`
    * `no-bold`
        * Removes a prior `bold` or `dim`.
    * `no-dim`
        * Removes a prior `dim` or `bold`.
    * `no-italic`
        * Removes a prior `italic`.
    * `no-underline`
        * Removes a prior `underline`.
    * `no-strikethrough`
        * Removes a prior `strikethrough`.
    * `fg=black`
    * `fg=red`
    * `fg=green`
    * `fg=orange`
    * `fg=blue`
    * `fg=magenta`
    * `fg=cyan`
    * `fg=white`
    * `fg=default`
    * `fg=``
        * Same as `fg=default`.
    * `bg=black`
    * `bg=red`
    * `bg=green`
    * `bg=orange`
    * `bg=blue`
    * `bg=magenta`
    * `bg=cyan`
    * `bg=white`
    * `bg=default`
    * `bg=`
        * Same as `bg=default`.

Limitations:

    * Square brackets cannot be used in the string that is being formatted.
        * This can probably be overcome with a better regex.
"""

import re


STYLE_REGULAR = "0"
STYLE_BOLD = "1"
STYLE_DIM = "2"
STYLE_ITALIC = "3"
STYLE_UNDERLINE = "4"
STYLE_STRIKETHROUGH = "9"
STYLE_NO_BOLD = "22"
STYLE_NO_DIM = "22"
STYLE_NO_ITALIC = "23"
STYLE_NO_UNDERLINE = "24"
STYLE_NO_STRIKETHROUGH = "29"
FG_BLACK = "30"
FG_RED = "31"
FG_GREEN = "32"
FG_ORANGE = "33"
FG_BLUE = "34"
FG_MAGENTA = "35"
FG_CYAN = "36"
FG_WHITE = "37"
FG_DEFAULT = "39"
BG_BLACK = "40"
BG_RED = "41"
BG_GREEN = "42"
BG_ORANGE = "43"
BG_BLUE = "44"
BG_MAGENTA = "45"
BG_CYAN = "46"
BG_WHITE = "47"
BG_DEFAULT = "49"

MODIFIER_STRING_TO_CODE = {
    "/": STYLE_REGULAR,
    "regular": STYLE_REGULAR,
    "bold": STYLE_BOLD,
    "dim": STYLE_DIM,
    "italic": STYLE_ITALIC,
    "underline": STYLE_UNDERLINE,
    "strikethrough": STYLE_STRIKETHROUGH,
    "no-bold": STYLE_NO_BOLD,
    "no-dim": STYLE_NO_DIM,
    "no-italic": STYLE_NO_ITALIC,
    "no-underline": STYLE_NO_UNDERLINE,
    "no-strikethrough": STYLE_NO_STRIKETHROUGH,
    "fg=black": FG_BLACK,
    "fg=red": FG_RED,
    "fg=green": FG_GREEN,
    "fg=orange": FG_ORANGE,
    "fg=blue": FG_BLUE,
    "fg=magenta": FG_MAGENTA,
    "fg=cyan": FG_CYAN,
    "fg=white": FG_WHITE,
    "fg=default": FG_DEFAULT,
    "fg=": FG_DEFAULT,
    "bg=black": BG_BLACK,
    "bg=red": BG_RED,
    "bg=green": BG_GREEN,
    "bg=orange": BG_ORANGE,
    "bg=blue": BG_BLUE,
    "bg=magenta": BG_MAGENTA,
    "bg=cyan": BG_CYAN,
    "bg=white": BG_WHITE,
    "bg=default": BG_DEFAULT,
    "bg=": BG_DEFAULT}

INLINE_FORMAT = re.compile(
    r"(?:\[([a-z\-= \/]*)\])|([^\[\]]+)")


def modifier_code_from_string(modifier_str: str) -> str:
    """Get the ANSI escape code for a given modifier."""
    modifier = MODIFIER_STRING_TO_CODE.get(modifier_str)
    if modifier is None:
        raise ValueError(f"Modifier `{modifier_str}` is not a valid modifier: "
                         f"{MODIFIER_STRING_TO_CODE.keys()}")
    return modifier


def escape_sequence_from_inline_modifiers(modifiers_str: str) -> str:
    """Create an ANSI escape sequence from an inline format string."""
    modifiers = modifiers_str.split()
    formats = [modifier_code_from_string(modifier)
               for modifier in modifiers]
    if len(formats) == 0:
        return ""
    formats.sort()
    return f"\x1b[{';'.join(formats)}m"


def format_inline_match(g0: str, g1: str):
    """Format a single inline match provided from `INLINE_FORMAT`."""
    if len(g0) != 0 and len(g1) != 0:
        raise ValueError("String matched both inline groups")
    if len(g0) != 0:
        return escape_sequence_from_inline_modifiers(g0)
    if len(g1) != 0:
        return g1
    raise ValueError("String had no inline matches")


def format_inline(s: str) -> str:
    """Format a string using modifiers specified inline.

    `s` is a string where style modifiers, such as foreground and background
    colour can be applied by providing a list of modifiers within a set of
    square brackets (like in a tmux config).

    For example:

        "This is my [bold underline fg=red]partially formatted string[/]"

    Limitations:
        * The string isn't allowed to contain the characters '[' or ']'.
    """
    return "".join([format_inline_match(g0, g1)
                    for g0, g1 in INLINE_FORMAT.findall(s)])
