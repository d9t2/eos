#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Submodule providing a simple API to print to conditionally log to stdout."""

from .format import format_inline


class Logger:
    """Class to print messages to stdout, conditionally based on severity."""

    SEVERITY_SILENT = 0
    SEVERITY_INFO = 1
    SEVERITY_ERROR = 2
    SEVERITY_WARNING = 3
    SEVERITY_DEBUG = 4

    def __init__(self, severity: int = SEVERITY_WARNING):
        """Initialise the printer object."""
        self.severity = severity

    def can_print_info(self):
        """Can this printer print items of severity `INFO`?"""
        return self.severity >= self.SEVERITY_INFO

    def can_print_error(self):
        """Can this printer print items of severity `ERROR`?"""
        return self.severity >= self.SEVERITY_ERROR

    def can_print_warning(self):
        """Can this printer print items of severity `WARNING`?"""
        return self.severity >= self.SEVERITY_WARNING

    def can_print_debug(self):
        """Can this printer print items of severity `DEBUG`?"""
        return self.severity >= self.SEVERITY_DEBUG

    def info(self, message: str):
        """Write a message with severity `INFO`."""
        if self.can_print_info():
            print(format_inline(message))

    def error(self, message: str):
        """Write a message with severity `ERROR`."""
        if self.can_print_error():
            print(format_inline(f"[bold fg=red]ERROR:[/] {message}"))

    def warning(self, message: str):
        """Write a message with severity `WARNING`."""
        if self.can_print_warning():
            print(format_inline(f"[bold fg=orange]WARNING:[/] {message}"))

    def debug(self, message: str):
        """Write a message with severity `DEBUG`."""
        if self.can_print_debug():
            print(format_inline(f"[bold fg=magenta]DEBUG:[/] {message}"))
