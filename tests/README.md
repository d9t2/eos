# Tests

`eos` is tested by setting up a minimal container where just `python3` has been
installed, and running `eos` repeatedly to install all packages listed in
`packages.json`.


## Sections

1. [Available Containers](#available-containers)
1. [Build Container](#build-container)
1. [Run Container](#run-container)
1. [Run Tests](#run-tests)


## Available Containers

Available containers:

  * `eos-test-ubuntu22_04`

    * Ubuntu 22.04.
    * Defined in `containers/ubuntu22_04.Containerfile`.


## Build Container

To build the container `eos-test-<name>`, from the top directory of the repo:

```bash
make -C tests <name>
```

If the build is successful, then the test for that container has passed.


## Run Container

To run the container `eos-test-<name>`, from the top directory of the repo:

```bash
make -C tests run-<name>
```


## Run Tests

To run all tests, from the top directory of the repo:

```bash
make -C tests
```

This will force build all available containers.
