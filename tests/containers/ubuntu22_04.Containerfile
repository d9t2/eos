# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
FROM ubuntu:22.04
# Install python
RUN apt-get update && apt-get install -y python3
# Ensure unbuffered python so stdout behaves properly
ENV PYTHONUNBUFFERED=1
# Move to repo
WORKDIR /eos
# Run installer: Package managers
RUN ./eos.py \
  --debug 5 \
  --distro-id ubuntu \
  --distro-version 22.04 \
  --install \
    curl \
    go \
    pip
RUN ./eos.py \
  --debug 5 \
  --distro-id ubuntu \
  --distro-version 22.04 \
  --install \
    cargo \
    pip:distro \
    pipx
ENV PATH=/root/.cargo/bin:$PATH
# Run installer: Terminal environment
RUN ./eos.py \
  --debug 5 \
  --install \
    alacritty-deps \
    bat \
    bottom \
    delta \
    dust \
    exa \
    fd-find \
    gitg \
    lazygit \
    mccabe \
    neovim-deps \
    nodejs \
    procs \
    pycodestyle \
    pydocstyle \
    pyflakes \
    python-lsp-server \
    ripgrep \
    rope \
    tldr \
    tmux \
    zsh \
    zsh-syntax-highlighting
RUN ./eos.py \
  --debug 5 \
  --install \
    alacritty \
    neovim \
    tree-sitter-cli
