# Manifest Format

There are four top level sections to the manifest:

* [`distros`](#distros)
* [`managers`](#managers)
* [`packages`](#packages)
* [`sudos`](#sudos)


## `distros`

```python
{
  "distros": {
    "distro-name": {
      "manager": "",
      "install": "",
      "sudo": bool
    }
  }
}
```

* `manager`: The name of the binary to use for package management on this
             distro.

  * This name must return a valid binary when passed to `which`.
  * For example, `apt-get` for Ubuntu, `pacman` for Arch Linux.

* `install`: The arguments to pass to `manager` to install packages.

  * This should include whatever flags are needed to disable user confirmation.
  * This should not include the `manager` binary name.
  * For example, `-y install` for `apt-get`, `-S --noconfirm` for `pacman`.

* `sudo`: `true` or `false` depending on whether `manager` requires superuser
          privileges.


## `managers`

```python
{
  "managers": {
    "manager-name": {
      "manager": "",
      "install": "",
      "sudo": bool
    }
  }
}
```

* `manager`: The name of the binary to use for this manager.

  * This can be omitted if it is the same as `manager-name`.
  * This name must return a valid binary when passed to `which`.
  * For example, `pip3` for `pip`.

* `install`: The arguments to pass to `manager` to install packages.

  * This should include whatever flags are needed to disable user confirmation.
  * This should not include the `manager` binary name.
  * For example, `install --user` for `pip`.

* `sudo`: `true` or `false` depending on whether `manager` requires superuser
  privileges.


## `packages`

```python
{
  "packages": {
    "package-name": {
      "distro-name": {
        "package": "",
        "minimum_version": ""
      },
      "manager-name": {
        "package": "",
        "repo": ""
      },
      "prefer": []
    }
  }
}
```

* `distro-name`: The name of a distro that can install this package. You can
                 provide entries for as many distros as you like.

  * `package`: The name of the package. This can be omitted if it is the same
               as `package-name`.
  * `minimum_version`: The minimum version of the distro for this package to be
                       supported on the appropriate package manager. This can
                       be omitted.

* `manager-name`: The name of a manager that can install this package. You can
                  provide entries for as many managers as you like.

  * `package`: The name of the package. This can be omitted if it is the same
               as `package-name`.
  * `repo`: The repository that holds the code that will be installed through
            this manager. This can be omitted.

* `prefer`: The list of sources the `package` can be acquired from in their
            order of preference.

  * This can include:

    * `manager` names (e.g. `cargo` and `pip`).
    * `distro` names (e.g. `ubuntu` and `arch`).
    * The word `"distro"` (this matches all distributions).


## `sudos`

```python
{
  "sudos": []
}
```

`sudos` is a list of names of binaries that can be used to obtain superuser
privileges. At least one name must be provided in order to use any manager that
requires `sudo`.
