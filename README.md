# eos - An installer to enable easy setup of a new OS

`eos` is a python utility to enable a user to setup a new Operating System more
easily.

Say you've just created a new Ubuntu 20.04 Virtual Machine and want to install
your development environment onto it. Lets pick some tools that you always like
to have on your system:

* [`bat`](https://github.com/sharkdp/bat)

  * This is on `apt` and `cargo`.

* [`exa`](https://github.com/ogham/exa)

  * This isn't on `apt` in 20.04, but is on `cargo`.

* [`tldr`](https://github.com/tldr-pages/tldr)

  * This has multiple implementations, including one for `cargo` and one for
    `pip`.

With `eos`, you can do the following, and `eos` will handle which package
manager is invoked (assuming the managers are all installed):

```bash
./eos.py --install bat exa tldr
```


## Sections

1. [Dependencies](#dependencies)
1. [Benefits](#benefits)
1. [Managers](#managers)
1. [Manifests](#manifests)
1. [Library Documentation](#library-documentation)
1. [Tests](#tests)
1. [Limitations](#limitations)
1. [Copyright](#copyright)


## Dependencies

Basic script functionality requires:

* `python3`

`pip` package installed via `ensurepip` requires:

* `python3-venv`

Other than these, each `package` is obviously dependent on its `manager`
existing.


## Benefits

* Once you have set up a manifest, you don't have to remember the different
  package names across multiple OSes. You can just provide `eos` with the
  basic, friendly name you chose when you wrote the manifest.
* You can reduce setting up packages across all managers on a new system to:

  ```bash
  git clone https://gitlab.com/d9t2/eos.git /opt/eos
  cd /opt/eos
  ./eos.py \
    --manifest \
      $your_manifest_file_0 \
      $your_manifest_file_1 \
      ... \
      $your_manifest_file_m \
    --install $package_0 $package_1 ... $package_n
  ```


## Managers

You can run the script with `--debug 5` and no activity arguments to get a
dump of what is in the loaded manifest:

```bash
./eos.py --debug 5
```


## Manifests

See [manifests/README.md](manifests/README.md).


## Library Documentation

See [docs/README.md](docs/README.md).


## Tests

See [tests/README.md](tests/README.md).


## Limitations

`eos` does not aim to be a package manager; it also doesn't aim to do the job
of [`mpm`](https://github.com/kdeldycke/meta-package-manager) or something
similar.

The user is expected to provide a manifest, that describes which package should
come from where under what circumstances. Once the user has made this once,
they can reuse it when making a new version of the OS they made it for.

If they specify the use of managers like `cargo` and `pip`, then this can be
used across various OSes.

There are also some further limitations due to current implementation:

* If you request to install a `package`, and it has no available `managers`,
  `eos` will exit with an unhelpful error message.

* `packages` can't define `dependencies`.

  * For example:

    * `alacritty` needs a bunch of programs from the distro package
      manager before it can be installed.
    * `lazygit` needs `go` on most distros, so if the install manager of choice
      is `go`, then `go` needs to be installed.

  * Ideally `packages` should be able to provide a list of `dependencies`.

    * This should also infer that the `manager` is a dependency.


## Copyright

See [LICENSE](LICENSE).
