# Documentation

`eos` is comprehensively documented in pydocstrings, and can optionally
generate `html` based documentation.


## Sections

1. [Dependencies](#dependencies)
1. [Generate](#generate)
1. [Clean](#clean)


## Dependencies

`eos` uses `pdoc` to generate documentation.

To install the dependencies required for documentation generation, from the top
directory of the repo:

```bash
make deps-docs
```

This script requires `pipx`, `pip3`, or `pip` (checked for in this order).


## Generate

To generate documentation, from the top directory of the repo:

```bash
make docs
```

The documentation can then be found at `docs/gen`.

Note: This command will delete any existing generated documentation before
generating again.


## Clean

To clean any generated documentation, from the top directory of the repo:

```bash
make -C docs clean
```
