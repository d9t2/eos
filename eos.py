#!/usr/bin/env python3

# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""A CLI program to assist in setting up a new linux distro with dev tools."""

import argparse
import pathlib
import sys
import traceback

from eos import Installer, Logger, Manifest, RationalisedManifest


DEFAULT_JSONS = [
    pathlib.Path(__file__).parent / "manifests" / "distros.json",
    pathlib.Path(__file__).parent / "manifests" / "managers.json",
    pathlib.Path(__file__).parent / "manifests" / "packages.json",
    pathlib.Path(__file__).parent / "manifests" / "sudos.json"]


def distro_id() -> str:
    """Get the distro id, safely, returning None if no `distro`."""
    try:
        import distro
    except ModuleNotFoundError:
        return None
    return distro.id()


def distro_version() -> str:
    """Get the distro version, safely, returning None if no `distro`."""
    try:
        import distro
    except ModuleNotFoundError:
        return None
    return distro.version()


def main(args: argparse.Namespace, logger: Logger):
    """Run the main CLI functions."""
    # Initial debug and manifest loading
    logger.debug(f"Distro ID: {args.distro_id}")
    logger.debug(f"Distro Version: {args.distro_version}")
    manifests = "\n  ".join([str(manifest) for manifest in args.manifest])
    logger.debug("Manifests:\n"
                 f"  {manifests}")
    manifest = Manifest.from_json_files(args.manifest)
    logger.debug("Manifests loaded")
    # Warnings
    if args.distro_id is None:
        logger.warning("Python module `distro` not found, can't infer distro "
                       "id. Provide `--distro-id`")
    if args.distro_version is None:
        logger.warning("Python module `distro` not found, can't infer distro "
                       "version. Provide `--distro-version`")
    # Rationalise the manifest
    if args.distro_id is None:
        raise ValueError("Distro ID could not be ascertained")
    manifest = RationalisedManifest(args.distro_id,
                                    args.distro_version,
                                    manifest)
    logger.debug("Rationalised Manifest:\n" +
                 manifest.brief_summary())
    # Create the installer
    installer = Installer(manifest, logger)
    # Do `--install-all` and quit
    if args.install_all:
        installer.install_packages(manifest.packages.keys())
        sys.exit(0)
    # Do `--install` and quit
    if args.install is not None and len(args.install) > 0:
        if isinstance(args.install, list):
            args.install = [arg for arg_list in args.install for arg in arg_list]
        installer.install_packages(args.install)
        sys.exit(0)


if __name__ == "__main__":
    # Get arguments
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--install",
        action="append",
        nargs="*",
        help="Install a program"
    )
    parser.add_argument(
        "--install-all",
        action="store_true",
        help="Install all programs from the manifest"
    )
    parser.add_argument(
        "--debug",
        type=int,
        default=Logger.SEVERITY_WARNING,
        help="Provide the log level to use"
    )
    parser.add_argument(
        "--distro-id",
        type=str,
        default=distro_id(),
        help="Override the distro id"
    )
    parser.add_argument(
        "--distro-version",
        type=str,
        default=distro_version(),
        help="Override the distro version"
    )
    parser.add_argument(
        "--manifest",
        action="append",
        nargs="*",
        type=pathlib.Path,
        default=DEFAULT_JSONS,
        help="Specify which manifests to use"
    )
    args, unknown = parser.parse_known_args()
    if len(unknown) > 0:
        print(f"Unknown arguments: {unknown}")
        sys.exit(1)
    # Run
    logger = Logger(args.debug)
    try:
        main(args, logger)
    except KeyboardInterrupt:
        logger.error("Ctrl-C")
        sys.exit(1)
    except Exception as err:
        if logger.can_print_debug():
            logger.error(traceback.format_exc())
        else:
            logger.error(f"{err}\n")
        sys.exit(1)
