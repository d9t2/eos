# Copyright (c) 2023 Dominic Adam Walters
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

at ?= @
this_dir := $(strip $(shell dirname "$(realpath $(lastword $(MAKEFILE_LIST)))"))

.PHONY: help
help:
	$(at)printf "%s\n" \
		"Usage:" \
		"  make           : Alias for \`make help\`." \
		"  make clean     : Clean the repo." \
		"  make deps-docs : Install dependencies for \`make docs\`." \
		"  make docs      : Generate HTML documentation." \
		"  make help      : Print this message."

.PHONY: deps-docs
deps-docs:
	$(at)$(MAKE) -C $(this_dir)/docs deps at=$(at)

.PHONY: docs
docs:
	$(at)$(MAKE) -C $(this_dir)/docs gen at=$(at)

.PHONY: clean
clean:
	$(at)$(MAKE) -C $(this_dir)/eos clean at=$(at)
	$(at)$(MAKE) -C $(this_dir)/docs clean at=$(at)
